$(() => {
    if ($('#dl-menu').length)
        $('#dl-menu').dlmenu({
            animationClasses: { classin: 'dl-animate-in-2', classout: 'dl-animate-out-2' }
        });
    $('.slick-banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1000,
        dots: false,
        autoplay: true,
        arrows: false
    });
    let banner = $('.slick-banner').find(`.slick-slide[data-slick-index=0]`);
    banner.find('.banner__content').addClass("active");
    $('.slick-banner').on('afterChange', function(event, slick, currentSlide) {
        let banner = $('.slick-banner').find(`.slick-slide[data-slick-index=${currentSlide}]`);
        $(".slick-slide").not(banner).find('.banner__content').removeClass("active");
        banner.find('.banner__content').addClass("active");
    });
    $('.company__slick').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        autoplay: true,
        speed: 700,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.slick-comment').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        arrows: false

    });
    $('.slick-project').slick({
        slidesToShow: 2,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToScroll: 1,
        arrows: true,
        autoplay: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }, ]
    });

    let scrollTop = $(".btn--to-top");
    $(window).on("scroll", function() {
        let topPos = $(this).scrollTop();
        if (topPos > 100) {
            $(scrollTop).css("opacity", "1");
           
			
        } else {
            $(scrollTop).css("opacity", "0");
           
        }
		
		 if ($(window).width() < 767) {
			 if (topPos > 100) {
				
				$('.btn-filter').css("display", "block");
				$('.filter-mobile').css("display", "block");
				
			} else {
				
				$('.btn-filter').css("display", "none");
				$('.filter-mobile').css("display", "none");
				$(".aside").removeClass("open");
			}
		 }

    });
    $(scrollTop).click(function() {
        $('html,body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('.slide--bottom').slick({
        speed: 300,

        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        loop: false,
        prevArrow: $('.slide--bottom-prev'),
        nextArrow: $('.slide--bottom-next'),
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.slide--bottom-1').slick({
        speed: 300,
        autoplay: true,

        slidesToScroll: 1,
        infinite: false,
        loop: false,
        prevArrow: $('.slide--bottom-prev-1'),
        nextArrow: $('.slide--bottom-next-1'),
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.slide--product').each(function() {
        let slick = $(this);
        slick.slick({
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            prevArrow: slick.siblings('.slide--top-prev'),
            nextArrow: slick.siblings('.slide--top-next')
        });
    });
    $('.slide--project').each(function() {
        let slick = $(this);
        slick.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            prevArrow: slick.next().find('.slide--top-prev'),
            nextArrow: slick.next().find('.slide--top-next')
        });
    });
    if ($('.achieve__counter').length)
        $('.achieve__counter').counterUp({
            delay: 10,
            time: 1000
        });

});